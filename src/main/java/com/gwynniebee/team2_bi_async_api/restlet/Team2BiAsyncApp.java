/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.team2_bi_async_api.restlet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.gwynniebee.async.job.helper.AsyncJobRestletServlet;
import com.gwynniebee.talend.jobs.bi.config.BIConfig;
import com.gwynniebee.team2_bi_job_api.restlet.resources.Team2BiJob;

/**
 * Barcode file upload service routing and controlling resources.
 * @author vijay
 */

public class Team2BiAsyncApp extends AsyncJobRestletServlet {
    private static final int NUM_THREADS = 3;

    /**
     * Team2BiAsyncApp.
     */
    public Team2BiAsyncApp() {
        super(NUM_THREADS, "", Team2BiJob.class);
    }

    @Override
    public void init() throws ServletException {
        try {
            BIConfig.init();
        } catch (IOException e) {
            System.out.println("Failed to initialize");
            e.printStackTrace();
        }
        super.init();
    }

    @Override
    public void destroy() {
        super.destroy();
    }

}
