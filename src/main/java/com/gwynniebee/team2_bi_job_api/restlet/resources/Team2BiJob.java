/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.team2_bi_job_api.restlet.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.gwynniebee.talend.jobs.TalendJobResourceBase;
import com.gwynniebee.talend.jobs.bi.config.BIConfig;
import com.gwynniebee.talend.objects.TalendProjectJob;

/**
 * emp_job class. To change this resource rename the class to the required
 * class.
 * @author vijay
 */

public class Team2BiJob extends TalendJobResourceBase {
    /**
     * Pipeline Name.
     */
    private static final String PIPELINE_NAME = "daily";

    /**
     * Estimated time to complete.
     */
    private static final int ESTIMATED_COMPLETION_TIME = 60;

    /**
     * Constructor.
     */
    public Team2BiJob() {
        super();
        // Make a vector of Jobs
        Vector<String> populateDailyTablesJobsVector = new Vector<String>();
        populateDailyTablesJobsVector.add("area_distribution_of_employees_with_stats");
        populateDailyTablesJobsVector.add("leave_report_with_stats");

        // Add jobs vector to Talend Project.
        TalendProjectJob populateDailyTableProject = new TalendProjectJob("/home/gb/share/team2_bi_report", populateDailyTablesJobsVector);
        // Make list of all Talend Projects.
        List<TalendProjectJob> talendProjectJobs = new ArrayList<TalendProjectJob>();
        talendProjectJobs.add(populateDailyTableProject);
        // Initialise using the list in previous step.
        // this.setJobPipelineName(PIPELINE_NAME);

        this.init(BIConfig.getConfDir(), BIConfig.getJobName(), talendProjectJobs, ESTIMATED_COMPLETION_TIME);
    }

    /**
     *@return string
     */
    public String getJobType() {
        return BIConfig.getJobName();
    }
}
