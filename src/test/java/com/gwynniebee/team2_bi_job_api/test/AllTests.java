/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.team2_bi_job_api.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.gwynniebee.team2_bi_job_api.test.serverresource.AllServerResourceTest;

/**
 * All test cases.
 * @author Jitender
 */
@RunWith(Suite.class)
@SuiteClasses({AllServerResourceTest.class})
public class AllTests {

}
