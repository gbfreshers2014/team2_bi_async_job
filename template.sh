#!/bin/sh
clear
new_project_name='team2_bi_async'
new_properties_name='job'
local_tomcat_path='/var/lib/tomcat7'
package_name='emp_job_api'
resource_name='emp_job'
application_name='Team2BiAsyncApp'
project_description='project-description'

echo "running templates replace on REST-template Project"
rm -rf .git/
echo "replacing project name \"team2_bi_async\" with \"$new_project_name\""
#grep -rl 'team2_bi_async' .
grep -rl 'team2_bi_async' . | xargs sed -i 's|team2_bi_async|'"$new_project_name"'|g'

echo "replacing \"job.properties\" with \"$new_properties_name.properties\""
#grep -rl 'job.properties' .
grep -rl 'job.properties' . | xargs sed -i 's|job.properties|'"$new_properties_name.properties"'|g'
find ./configuration -name 'job.properties' -exec rename 'job.properties' $new_properties_name.properties '{}' \;

echo "replacing \"/var/lib/tomcat7\" with $local_tomcat_path"
#grep -rl '/var/lib/tomcat7'
grep -rl '/var/lib/tomcat7' . | xargs sed -i 's|/var/lib/tomcat7|'"$local_tomcat_path"'|g'

echo "replacing the description from \"{project-description}\" to \"$project_description\" in pom.xml"
sed -i 's|{project-description}|'"$project_description"'|g' pom.xml

echo "rearranging the files"
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/{resource-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/'"$resource_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/{application-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/'"$application_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/' src/main/java/com/gwynniebee/$package_name/
mv -v 'src/test/java/com/gwynniebee/{package-name}/' src/test/java/com/gwynniebee/$package_name/

echo "changing the package name from \"{package-name}\" to \"$package_name\""
grep -rl '{package-name}' ./src
grep -rl '{package-name}' ./src | xargs sed -i 's|{package-name}|'"$package_name"'|g'

echo "replacing the resource name from \"RestTemplateResource\" to \"$resource_name\""
grep -rl '{resource-name}' ./src
grep -rl '{resource-name}' ./src | xargs sed -i 's|{resource-name}|'"$resource_name"'|g'

echo "replacing the Application name from \"{GBRestTemplateApplication}\" to \"$application_name\""
grep -rl '{application-name}' ./src
grep -rl '{application-name}' ./src | xargs sed -i 's|{application-name}|'"$application_name"'|g'

#rm template.sh~ template.sh
#end of shell script
#exit 0
